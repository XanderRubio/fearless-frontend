## Fearless Frontend Demo
![Fearless_Front-End](/uploads/8395c63407cca2cd99218d9c57efae5c/Fearless_Front-End.mp4)

## Overview
Fearless Frontend is a versatile event planning tool that allows organizations to host both in-person and virtual conferences for large groups of participants and presenters. It is built using various tech tools such as Python, JavaScript, Django, React, HTML, and CSS. With Fearless Frontend, creating new conferences, presentations, attendees, and locations is easy. Presenters receive email notifications via RabbitMQ to confirm presentation approval or rejection. Furthermore, the application incorporates third-party APIs for conference location photos and weather data based on coordinates. Originally developed as a monolith, it was later transformed into 7 Docker containers and three microservices for improved scalability and server load balancing.

For seamless conference management that reduces server crashes and downtime, I created a web-based conference management application for attendees and organizers. I also built an application-programmatic interface that sends JSON and returns JSON response objects.

## Initialization
To start with the application, follow the following steps:

1. Simply fork and clone the repository to your local system. 

2. Then, open a terminal and run the command "docker-compose build" to build the necessary images. 

3. Once the build process is complete, run the command "docker-compose up" to start the containers and get the application up and running. 

Please take some time to get to know the project and its features by exploring it thoroughly.
